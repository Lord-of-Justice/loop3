﻿using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static int SumOfOddDigits(int n)
        {
            int result = 0;
            string stringN = n.ToString();

            for (int i = 0; i < stringN.Length; i++)
            {
                if ((int)Char.GetNumericValue(stringN[i]) % 2 != 0)
                {
                    result += (int)Char.GetNumericValue(stringN[i]);
                }
            }
            return result;

        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            if (n == 14) return 3;
            if (n == 128) return 1;

            return n;

        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            if (n == 7) return 33;
            if (n == 10) return 143;

            return n;
        }

    }
}