using NUnit.Framework;

namespace LoopTasks.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void SumOfOddDigits_ReturnsCorrectValue()
        {
            void Test(int n, int result)
            {
                var actualResult = LoopTasks.SumOfOddDigits(n);

                Assert.AreEqual(result, actualResult);
            }

            Test(1, 1);
            Test(2, 0);
            Test(1234, 4);
            Test(246, 0);
        }


    }
}